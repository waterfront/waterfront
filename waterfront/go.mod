module gitlab.com/waterfront/waterfront

go 1.16

require (
	entgo.io/ent v0.8.1-0.20210630104730-a19a89a141cf
	github.com/go-chi/chi/v5 v5.0.3
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0 // indirect
	github.com/jackc/pgx/v4 v4.13.0
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
