package auth

import (
	"context"
	"log"
	"net/http"
	"strings"
)

//Middlewares used by the auth routes are declared here, then imported in routes.go

func Validate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearer := strings.Split(r.Header.Get("Authorization"), "Bearer ")
		if len(bearer) > 1 {
			valid, claims := validateToken(bearer[1])
			if !valid {
				log.Println("Token not valid")
				http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
				return
			}
			ctx := context.WithValue(r.Context(), "claims", claims)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			log.Println("Bad bearer len")
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}
	})
}

func Private(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := r.Context()
		claims, ok := ctx.Value("claims").(*tokenClaims)
		if !ok {
			log.Println("Error getting claims from context")
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}
		if claims.Username != "admin" {
			log.Println("Username is not admin")
			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
			return
		}
		next.ServeHTTP(w, r)
	})
}
