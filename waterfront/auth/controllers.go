package auth

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/waterfront/waterfront/db"
	"gitlab.com/waterfront/waterfront/ent"
	"gitlab.com/waterfront/waterfront/ent/user"
	"gitlab.com/waterfront/waterfront/utils"
	response "gitlab.com/waterfront/waterfront/utils"
	"gitlab.com/waterfront/waterfront/utils/errors"
	"golang.org/x/crypto/argon2"
)

//Here is where you write all your business logic
var env map[string]string = utils.InjectEnv()

type hashParams struct {
	memory      uint32
	iterations  uint32
	parallelism uint8
	saltLength  uint32
	keyLength   uint32
}

type tokenClaims struct {
	Username string `json:"username"`
	Role     string `json:"role"`
	jwt.StandardClaims
}

func login(w http.ResponseWriter, r *http.Request) {
	//We get the database
	var dbCtx, dbClient = db.Init()

	defer dbClient.Close()

	userStruct := ent.User{}

	//We decode the request body into the user
	err := json.NewDecoder(r.Body).Decode(&userStruct)
	if errors.Handle(w, r, err, 400) {
		return
	}

	//Do I need to comment all this?!

	userResp, err := dbClient.User. // UserClient.
					Query(). // User query builder.
					Where(user.Username(userStruct.Username)).
					Only(dbCtx) //We expect only one user

	if errors.Handle(w, r, err, 500) {
		return
	}

	correctPassword, err := comparePasswordAndHash(userStruct.Password, userResp.Password)

	if correctPassword == true {
		claims := tokenClaims{
			userResp.Username,
			userResp.Role.String(),
			jwt.StandardClaims{
				Audience:  "waterfront",
				ExpiresAt: time.Now().Local().Add(time.Minute * time.Duration(10)).Unix(),
				Id:        "",
				IssuedAt:  time.Now().Unix(),
				Issuer:    "waterfront",
				NotBefore: 0,
				Subject:   "",
			},
		}
		token, err := newToken(claims)
		if errors.Handle(w, r, err, 500) == true {
			return
		} else {
			payload := map[string]string{
				"token": token,
			}
			w.Write(response.Render(response.SUCCESS, "", payload))
		}
	} else {
		w.Write(response.Render(response.ERROR, "incorrect username or password"))
	}
	return
}

func register(w http.ResponseWriter, r *http.Request) {
	//We get the database
	var dbCtx, dbClient = db.Init()

	defer dbClient.Close()

	//We create a new user
	userCreate := dbClient.User.Create()
	userStruct := ent.User{}

	//We decode the request body into the user
	err := json.NewDecoder(r.Body).Decode(&userStruct)

	//Since they don't support creating an entity with a struct, we use generated methods.
	//See: https://github.com/ent/ent/issues/761

	p := hashParams{
		memory:      64 * 1024,
		iterations:  3,
		parallelism: 2,
		saltLength:  16,
		keyLength:   32,
	}

	//Hash the password
	hash, err := hashPassword(userStruct.Password, p)
	if errors.Handle(w, r, err, 500) {
		return
	}

	//Save the user
	user, err := userCreate.SetUsername(userStruct.Username).SetPassword(hash).Save(dbCtx)

	//looking for data validation? ent handles all that, see ent/schema/user.go

	//We handle the error
	if !(errors.Handle(w, r, err, 500)) {
		res, _ := json.Marshal(user)
		w.Write(res)
	}
	return
}

func hashPassword(password string, p hashParams) (string, error) {
	salt := generateRandomBytes(p.saltLength)

	hash := argon2.IDKey([]byte(password), salt, p.iterations, p.memory, p.parallelism, p.keyLength)

	// Base64 encode the salt and hashed password.
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	// Return a string using the standard encoded hash representation.
	encodedHash := fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s", argon2.Version, p.memory, p.iterations, p.parallelism, b64Salt, b64Hash)

	return encodedHash, nil
}

func generateRandomBytes(n uint32) []byte {
	b := make([]byte, n)
	rand.Read(b)
	return b
}

func comparePasswordAndHash(password, encodedHash string) (bool, error) {
	// Extract the parameters, salt and derived key from the encoded password
	// hash.
	p, salt, hash, err := decodeHash(encodedHash)
	if err != nil {
		return false, err
	}

	// Derive the key from the other password using the same parameters.
	otherHash := argon2.IDKey([]byte(password), salt, p.iterations, p.memory, p.parallelism, p.keyLength)

	// Check that the contents of the hashed passwords are identical. Note
	// that we are using the subtle.ConstantTimeCompare() function for this
	// to help prevent timing attacks.
	if subtle.ConstantTimeCompare(hash, otherHash) == 1 {
		return true, nil
	}
	return false, nil
}

func decodeHash(encodedHash string) (p *hashParams, salt, hash []byte, err error) {
	vals := strings.Split(encodedHash, "$")
	if len(vals) != 6 {
		return nil, nil, nil, errors.New("the encoded hash is not in the correct format")
	}

	var version int
	_, err = fmt.Sscanf(vals[2], "v=%d", &version)
	if err != nil {
		return nil, nil, nil, err
	}
	if version != argon2.Version {
		return nil, nil, nil, errors.New("incompatible version of argon2")
	}

	p = &hashParams{}
	_, err = fmt.Sscanf(vals[3], "m=%d,t=%d,p=%d", &p.memory, &p.iterations, &p.parallelism)
	if err != nil {
		return nil, nil, nil, err
	}

	salt, err = base64.RawStdEncoding.Strict().DecodeString(vals[4])
	if err != nil {
		return nil, nil, nil, err
	}
	p.saltLength = uint32(len(salt))

	hash, err = base64.RawStdEncoding.Strict().DecodeString(vals[5])
	if err != nil {
		return nil, nil, nil, err
	}
	p.keyLength = uint32(len(hash))

	return p, salt, hash, nil
}

func newToken(claims tokenClaims) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signingKey := []byte(env["SECRET"])

	// Sign and get the complete encoded token as a string using the secret

	tokenString, err := token.SignedString(signingKey)
	if err != nil {
		log.Println(err.Error())
		return "", err
	}
	return tokenString, nil
}

func validateToken(tokenString string) (bool, *tokenClaims) {
	claims := &tokenClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (i interface{}, err error) {
		return []byte(env["SECRET"]), nil
	})

	if claims, ok := token.Claims.(*tokenClaims); ok && token.Valid {
		return true, claims
	}
	log.Println(err)
	return false, nil
}
