package auth

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

//Middlewares exclusively used by the auth router, declared in middlewares.go
func middlewares(r chi.Router) {
	//r.Use(AdminOnly)
}

//Routes are declared here, controllers are declared in controllers.go
func routes(r chi.Router) {
	r.Post("/register", register)
	r.Post("/login", login)
}

//The AuthRouter creates the router, then adds the middlewares and routes
//Global middlewares can be passed to the router when called from main.go
func Router(middlewareSlices ...[]func(next http.Handler) http.Handler) chi.Router {
	//The router is created
	r := chi.NewRouter()
	//We import the middlewares in parameters, if any
	for _, middlewares := range middlewareSlices {
		for _, middleware := range middlewares {
			r.Use(middleware)
		}
	}
	//We import service specific middlewares
	middlewares(r)
	//We import service specific routes
	routes(r)
	//We return the router
	return r
}
