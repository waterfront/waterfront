package main

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/waterfront/waterfront/auth"
	"gitlab.com/waterfront/waterfront/utils"
)

//Global middlewares can be passed to service routers
func globalMiddlewares() []func(next http.Handler) http.Handler {
	middlewares := []func(next http.Handler) http.Handler{
		middleware.RequestID,
		middleware.RealIP,
		middleware.Logger,
		middleware.Recoverer,
		middleware.Timeout(60 * time.Second),
		middleware.SetHeader("Content-Type", "application/json"),
	}
	return middlewares
}

//Service routers are mounted here to the main rooter
//Each Mount = a service added to our app
func routes(r chi.Router) {
	r.Mount("/auth", auth.Router(globalMiddlewares()))

	//Example of a restricted/private route using the auth middleware
	//This route does not have the global middlewares, it's for example only, routes should
	//always be declared in corresponding services
	r.Route("/restricted", func(r chi.Router) {
		r.Use(auth.Validate)
		r.Use(auth.Private)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("hi"))
		})
	})
}

func main() {
	//We load env. variables
	env := utils.InjectEnv()

	//We create the main router
	r := chi.NewRouter()

	//We import our routes
	routes(r)

	//We start the server
	http.ListenAndServe(env["IP"]+":"+env["PORT"], r)
}
