package db

import (
	"context"
	"database/sql"
	"log"

	"entgo.io/ent/dialect"
	entsql "entgo.io/ent/dialect/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/waterfront/waterfront/ent"
)

// Open new connection
func Open(databaseUrl string) *ent.Client {
	db, err := sql.Open("pgx", databaseUrl)
	if err != nil {
		log.Fatal(err)
	}

	// Create an ent.Driver from `db`.
	drv := entsql.OpenDB(dialect.Postgres, db)
	return ent.NewClient(ent.Driver(drv))
}

//Call this whereever you need yo access the database
func Init() (context.Context, *ent.Client) {
	client := Open("postgresql://waterfront:password@database/waterfront")

	//We create an empty context
	ctx := context.Background()

	//We append the db client to the context
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatal(err)
	}

	//We return the context object
	return ctx, client
}
