package utils

import (
	"log"

	"github.com/joho/godotenv"
)

func InjectEnv() map[string]string {
	var env map[string]string
	env, err := godotenv.Read("/usr/bin/.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	return env
}
