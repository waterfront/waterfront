package errors

import (
	"encoding/json"
	"errors"
	"net/http"
)

type ErrResponse struct {
	Status         string `json:"status,omitempty"`
	Err            error  `json:"-"`                    // low-level runtime error
	HTTPStatusCode int    `json:"statusCode,omitempty"` // http response status code
	AppCode        int64  `json:"code,omitempty"`       // application-specific error code
	ErrorText      string `json:"error,omitempty"`      // application-level error message, for debugging
}

func Handle(w http.ResponseWriter, r *http.Request, err error, errCode int) bool {
	if err != nil {
		errResponse := ErrResponse{
			Status:         "error",
			Err:            err,
			HTTPStatusCode: errCode,
			ErrorText:      err.Error(),
		}
		response, _ := json.Marshal(errResponse)
		w.WriteHeader(errCode)
		w.Write(response)
		return true
	} else {
		return false
	}
}

func New(message string) error {
	return errors.New(message)
}
