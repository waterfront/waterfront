package utils

import (
	"encoding/json"
	"log"
)

//We define some standard response structs & helper functions

const (
	SUCCESS = "success"
	ERROR   = "error"
)

type Response struct {
	Status  string            `json:"status,omitempty"`
	Message string            `json:"message,omitempty"`
	Data    map[string]string `json:"data,omitempty"`
}

func Render(status string, message string, payload ...map[string]string) []byte {
	if len(payload) == 1 {
		resp, err := json.Marshal(Response{
			Status: status,
			Data:   payload[0],
		})
		if err != nil {
			log.Println(err.Error())
			return nil
		}
		return resp
	} else {
		resp, err := json.Marshal(Response{
			Status:  status,
			Message: message,
		})
		if err != nil {
			log.Println(err.Error())
			return nil
		}
		return resp
	}
}
